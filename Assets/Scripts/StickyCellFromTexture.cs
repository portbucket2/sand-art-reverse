using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class StickyCellFromTexture : APBehaviour
{
    public SandBucket sandBucket;
    public LevelData[] levelDatas;
    public GameObject cellPrefab, colorPalletPrefab, backgroundObj, forgroundObj;
    public Transform colorPalletHolder;
    public MeshFilter stickerMeshFilter;
    public Material backgroundMaterial, forgroungMaterial;
    public Image referenceImageUI;
    public SkinnedMeshRenderer peelingSticker;

    public float gap = 1;
    public bool cellForBlackColor = true;

    List<Collider> colliders = new List<Collider>();
    LevelData currentLevelData;
    float peelShapeTop = 0, peelShapeBottom = 0;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        if (gameState.Equals(GameState.GAME_PLAY_ENDED))
        {
            peelingSticker.SetBlendShapeWeight(1, peelShapeTop);
            peelingSticker.SetBlendShapeWeight(0, peelShapeBottom);
        }

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        if (Input.GetMouseButtonUp(0))
        {
            for (int i = 0; i < colliders.Count; i++)
            {
                colliders[i].enabled = true;
            }
        }
    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnGameInitializing()
    {
        base.OnGameInitializing();

        currentLevelData = levelDatas[gameManager.GetModedLevelNumber()];

        CreateSticyCells();
        CreateColorPallet();
        //CreateHoldInStickerMesh();        
    }

    public override void OnGameOver()
    {
        base.OnGameOver();

        peelShapeTop = 0;
        peelShapeBottom = 0;
        forgroundObj.SetActive(true);
        backgroundObj.SetActive(true);
        DOTween.To(() => peelShapeTop, x => peelShapeTop = x, 100, ConstantManager.DEFAULT_ANIMATION_TIME / 2)
            .SetEase(Ease.Linear)
            .OnComplete(()=> {

                DOTween.To(() => peelShapeBottom, x => peelShapeBottom = x, 100, ConstantManager.DEFAULT_ANIMATION_TIME /2)
                    .SetEase(Ease.Linear)
                    .OnComplete(() => {

                        peelingSticker.GetComponent<Renderer>().enabled = false;
                    });
            });

    }

    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void OnMatchImage()
    {
        gameplayData.isGameoverSuccess = true;
        gameManager.ChangeGameState(GameState.GAME_PLAY_ENDED);
    }

    void CreateColorPallet()
    {
        colorPalletHolder.DestroyAllChild();
        for (int i = 0; i < currentLevelData.palletColors.Length; i++)
        {
            GameObject go = Instantiate(colorPalletPrefab, colorPalletHolder);
            Image image = go.GetComponent<Image>();
            image.color = currentLevelData.palletColors[i];
            go.GetComponent<Button>().onClick.AddListener(()=> {

                sandBucket.SelectColor(image);
            });

        }
    }

    void CreateHoldInStickerMesh()
    {
        Mesh mesh = stickerMeshFilter.mesh;
        List<Vector3> vertices = new List<Vector3>();
        vertices = mesh.vertices.ToList();
        List<int> triangles = new List<int>();
        triangles = mesh.triangles.ToList();

        int name = 1;
        for (int i = 0; i < triangles.Count; i += 3)
        {
            Vector3 v0 = vertices[triangles[i] + 0];
            Vector3 v1 = vertices[triangles[i] + 1];
            Vector3 v2 = vertices[triangles[i] + 2];

            Vector3 triangleCenter = (v0 + v1 + v2) / 3;
            triangleCenter = v0;
            GameObject go = new GameObject("Check_" + name++);
            go.transform.position = triangleCenter;
            RaycastHit hit;
            if(Physics.Raycast(new Ray(triangleCenter, new Vector3(-45, 0, 0)), out hit, 10f,  1 << ConstantManager.SENSOR))
            {
            }

        }
    }

    void CreateSticyCells()
    {
        Color black = new Color(0, 0, 0, 1);
        Color white = new Color(1, 1, 1, 1);

        int cellCount = 0;
        Texture2D stickerTexture = currentLevelData.blackTexture.texture;
        backgroundMaterial.SetTexture("_BaseMap", null);
        forgroungMaterial.SetTexture("_BaseMap", null);
        Texture2D test = new Texture2D(1000, 1000);
        backgroundMaterial.SetTexture("_BaseMap", currentLevelData.backgroundTexture.texture);
        forgroungMaterial.SetTexture("_BaseMap", currentLevelData.foregroundTexture.texture);
        backgroundObj.SetActive(false);
        forgroundObj.SetActive(false);

        referenceImageUI.sprite = currentLevelData.colorTexture;

        for (int row = 0; row < stickerTexture.width; row++)
        {
            for (int column = 0; column < stickerTexture.height; column++)
            {
                Color color = stickerTexture.GetPixel(row, column);
                color.a = 1;

                if (cellForBlackColor)
                {
                    if (color.Equals(black))
                    {
                        GameObject go = Instantiate(cellPrefab, transform);
                        go.name = row + "_" + column;
                        go.transform.localPosition = new Vector3((row - stickerTexture.width / 2) * gap, (column - stickerTexture.height / 2) * gap, 0);

                        colliders.Add(go.transform.GetChild(0).GetComponent<Collider>());
                        cellCount++;
                    }
                }
                else if (!color.Equals(white))
                {
                    GameObject go = Instantiate(cellPrefab, transform);
                    go.name = row + "_" + column;
                    go.transform.localPosition = new Vector3((row - stickerTexture.width / 2) * gap, (column - stickerTexture.height / 2) * gap, 0);

                    colliders.Add(go.transform.GetChild(0).GetComponent<Collider>());
                    cellCount++;
                }
            }
        }
        transform.eulerAngles = transform.eulerAngles.ModifyThisVector(90, 0, 0);

        Debug.LogError("Total Cell Count: " + cellCount);
    }
    public static Texture2D textureFromSprite(Sprite sprite)
    {
        if (sprite.rect.width != sprite.texture.width)
        {
            Texture2D newText = new Texture2D((int)sprite.rect.width, (int)sprite.rect.height);
            Color[] newColors = sprite.texture.GetPixels((int)sprite.textureRect.x,
                                                         (int)sprite.textureRect.y,
                                                         (int)sprite.textureRect.width,
                                                         (int)sprite.textureRect.height);
            newText.SetPixels(newColors);
            newText.Apply();
            return newText;
        }
        else
            return sprite.texture;
    }
    #endregion ALL SELF DECLEAR FUNCTIONS

}

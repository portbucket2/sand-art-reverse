using UnityEngine;
using System;

[Serializable]
public class LevelData
{
    public Sprite backgroundTexture, foregroundTexture, blackTexture, colorTexture;
    public Color[] palletColors = new Color[]{
        Color.black,
        Color.cyan,
        Color.red,
        Color.green,
        Color.blue,
        Color.yellow
    };
}
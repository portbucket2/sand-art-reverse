using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandPiece : APBehaviour
{
    public Rigidbody rig;
    public bool isCollided;

    SandBucket sandBucket;
    public Color particleColor;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
        if (sandBucket == null) sandBucket = FindObjectOfType<SandBucket>();
    }

    public override void OnEnable()
    {
        base.OnEnable();

        isCollided = false;
        rig.velocity = Vector3.zero;
        rig.angularVelocity = Vector3.zero;
        rig.isKinematic = false;
        particleColor = sandBucket.currentColor;
    }

    public override void OnDisable()
    {
        base.OnDisable();

        isCollided = false;
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer.Equals(ConstantManager.SENSOR))
        {
            other.GetComponent<Collider>().enabled = false;
            other.GetComponent<Renderer>().material.color = particleColor;
            Reset();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer.Equals(ConstantManager.SAND_MESH))
        {
            if (sandBucket == null) sandBucket = FindObjectOfType<SandBucket>();
            sandBucket.OnCollisionEnterCallback(this);
        }
    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    internal void Reset()
    {
        rig.velocity = Vector3.zero;
        rig.angularVelocity = Vector3.zero;
        rig.isKinematic = true;
        APTools.poolManager.Destroy(gameObject);
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}

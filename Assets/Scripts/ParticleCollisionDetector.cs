using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class ParticleCollisionDetector : APBehaviour
{
    public Color particleColor;
    public Transform cellHolder;

    ParticleSystem _particleSystem;

    bool firstTime = false;
    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();

        //events = new List<ParticleCollisionEvent>();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();
    }

    void Update()
    {
        //if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        //{
        //    gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
        //    gameState = GameState.GAME_PLAY_STARTED;
        //}

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    private void OnParticleCollision(GameObject other)
    {   
        if (other.layer.Equals(ConstantManager.SENSOR))
        {
            other.GetComponent<Collider>().enabled = false;
            other.GetComponent<Renderer>().material.color = particleColor;
        }

        // set
        //_particleSystem.SetTriggerParticles(ParticleSystemTriggerEventType.Enter, enter);
        //ParticlePhysicsExtensions.GetCollisionEvents(_particleSystem, other, events);

        //foreach (ParticleCollisionEvent item in events)
        //{
        //    //Debug.LogError(item.colliderComponent.name);
        //    if (item.colliderComponent.gameObject.layer.Equals(ConstantManager.SAND_CANVAS) && !item.colliderComponent.name.Equals("Ground"))
        //    {
        //        Debug.LogError("Sensor");
        //        //item.colliderComponent.gameObject.SetActive(false);
        //        item.colliderComponent.transform.Translate(0, Time.deltaTime, 0);
        //    }
        //}

        //allGererated_Particles = new ParticleSystem.Particle[_particleSystem.main.maxParticles];

        //int numCollisionEvents = _particleSystem.GetCollisionEvents(other, events);

        ////Make sure a particle exists first....
        //if (_particleSystem.GetParticles(allGererated_Particles) > 0)
        //{
        //    //Stop the particle from generating more than one so we move it to somewhere else so it wont collide
        //    allGererated_Particles[numCollisionEvents - 1].position = Vector3.zero;
        //    allGererated_Particles[numCollisionEvents - 1].remainingLifetime = -1;

        //    //Update the particle system with the new position
        //    _particleSystem.SetParticles(allGererated_Particles);
        //}

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void Initialize(Color color)
    {
        firstTime = true;
        particleColor = color;

        _particleSystem = GetComponent<ParticleSystem>();
        var main = _particleSystem.main;
        main.startColor = color;
        gameObject.GetComponent<Renderer>().material.color = color;
    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}

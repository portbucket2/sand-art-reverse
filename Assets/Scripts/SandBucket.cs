using UnityEngine;
using UnityEngine.UI;

public class SandBucket : APBehaviour
{
    public MeshFilter meshFilter;
    public MeshDoctor meshDoctor;

    public Color currentColor = Color.yellow;
    // Start is called before the first frame update
    public override void Start()
    {
        meshDoctor.UpdateMesh(meshDoctor.originalVertices, meshDoctor.originalTriangles, meshDoctor.originalUVs);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnCollisionEnterCallback(SandPiece sandPiece)
    {
        if (!sandPiece.isCollided)
        {
            sandPiece.isCollided = true;

            UpdateBody(sandPiece);
            //APTools.functionManager.ExecuteAfterSecond(0.5f, ()=> {

            //});
        }
    }

    public void UpdateBody(SandPiece sandPiece)
    {
        Vector3 newVertexPosition = sandPiece.transform.position;
        
        RaycastHit hit;
        if(Physics.Raycast(new Ray(newVertexPosition, Vector3.down), out hit, 5f, 1 << ConstantManager.SAND_MESH))
        {
            meshDoctor.PaintMesh(hit.point, sandPiece.particleColor, 100f);
            //meshDoctor.InfluenceTriangle(hit.triangleIndex, hit.normal, 0.0025f);
            //meshDoctor.DeleteTriangle(hit.triangleIndex);
            CalculateUV();
        }

        sandPiece.Reset();
    }

    void CalculateUV()
    {
        Debug.LogError("UV Not Calculated.");
    }

    public void SelectColor(Image image)
    {
        currentColor = image.color;
    }

}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshBender : MonoBehaviour
{
    public MeshFilter meshFilter;

    Mesh mesh;
    Vector3[] originalVertices;
    int[] originalTriangles;
    // Start is called before the first frame update
    void Start()
    {
        mesh = meshFilter.mesh;
        originalVertices = mesh.vertices;
        originalTriangles = mesh.triangles;

    }

    // Update is called once per frame
    void Update()
    {
        Vector3[] verts = new Vector3[originalVertices.Length];
        int[] tris = new int[originalTriangles.Length];
        System.Array.Copy(originalVertices, verts, originalVertices.Length);
        System.Array.Copy(originalTriangles, tris, originalTriangles.Length);

        for (int i = 0; i < tris.Length; i += 3)
        {
            Vector3 v0 = verts[tris[i]];

            Vector3 DotResult = Vector3.Cross(transform.forward, v0);
            if (DotResult.z > 0)
            {
                //VectorResult = transform.forward + checkingVert;
                v0.y += 1;
                verts[tris[i]] = v0;
                //Vector3 v1 = originalVertices[originalTriangles[i * 3 + 1]];
                //v1.y += DotResult;
                //verts[originalTriangles[i * 3 + 1]] = v1;
                //Vector3 v2 = originalVertices[originalTriangles[i * 3 + 2]];
                //v2.y += DotResult;
                //verts[originalTriangles[i * 3 + 2]] = v2;
            }
            else
            {
                //VectorResult = transform.forward - checkingVert;
            }
        }

        mesh.vertices = verts;
        meshFilter.mesh = mesh;
    }
}

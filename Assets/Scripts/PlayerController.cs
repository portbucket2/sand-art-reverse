using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : APBehaviour
{
    public GameObject sandPiecePrefab;
    public Transform currentSandPot, sandSpanPoint;
    public Transform sandHitPoint;
    public Vector3 sandPotOffset;

    [Range(0.0001f, 0.1f)]
    public float dragSensitivity = 0.0001f;

    public int sandRateOverTime = 10;

    Vector3 newPosition;
    bool dragging;

    #region ALL UNITY FUNCTIONS

    // Awake is called before Start
    public override void Awake()
    {
        base.Awake();
    }

    // Start is called before the first frame update
    public override void Start()
    {
        base.Start();

        //_particleSystem.Stop();
    }

    void Update()
    {
        if (gameState.Equals(GameState.GAME_INITIALIZED) && Input.GetMouseButtonDown(0))
        {
            gameManager.ChangeGameState(GameState.GAME_PLAY_STARTED);
            gameState = GameState.GAME_PLAY_STARTED;

            //_particleSystem.Play();
        }

        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

        dragging = Input.GetMouseButton(0);
        if (dragging)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition.ModifyThisVector(sandPotOffset));
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 10000, 1 << ConstantManager.SAND_PIECE))
            {
                sandHitPoint.transform.position = hit.point;

                float range = 0.1f;
                for (int i = 0; i < 5; i++)
                {
                    GameObject go = APTools.poolManager.Instantiate(sandPiecePrefab);
                    go.transform.parent = null;
                    go.transform.position = sandSpanPoint.position.ModifyThisVector(
                        Random.Range(-range, range),
                        Random.Range(-range, range),
                        Random.Range(-range, range)
                        );
                }
            }
        }

        currentSandPot.gameObject.SetActive(dragging);
    }

    void FixedUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    void LateUpdate()
    {
        if (!gameState.Equals(GameState.GAME_PLAY_STARTED))
            return;

    }

    #endregion ALL UNITY FUNCTIONS
    //=================================   
    #region ALL OVERRIDING FUNCTIONS

    public override void OnDrag(Vector3 dragAmount)
    {
        base.OnDrag(dragAmount);

        Vector3 mousePosition = Input.mousePosition;

        newPosition = currentSandPot.position.ModifyThisVector(dragAmount.x * dragSensitivity, 0, 0);
        newPosition.x = Mathf.Clamp(newPosition.x, -5, 5);
        newPosition.y = 5;

    }


    #endregion ALL OVERRIDING FUNCTIONS
    //=================================
    #region ALL SELF DECLEAR FUNCTIONS

    public void SelectColor(Image image)
    {

    }

    #endregion ALL SELF DECLEAR FUNCTIONS

}
